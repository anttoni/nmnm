<?php

require 'files/functions.php';
require 'vendor/autoload.php';

$pages = [

	'home' => [
		'title' => 'Welcome to my homepage',
		'nav' => 'home',
		'file' => 'home',
	],
	'iam-anttoni' => [
		'title' => 'I am Anttoni',
		'file' => 'about',
		'nav' => 'about',

	],
	'what-dones' => [
		'title' => 'What I have done',
		'file' => 'works',
		'nav' => 'works',
	],
	'blog' => [
		'title' => 'Writings',
		'file' => 'blog',
		'nav' => 'blog',
	],
	'interests' => [
		'title' => 'Interesting stuff',
		'file' => 'interests',
		'nav' => 'interests',
	],
	'hire' => [
		'title' => 'Hire Me!',
		'file' => 'hire',
	],
	'coffee' => [
		'title' => 'let\'s sit down and chat',
		'file' => 'coffee',
	],
	'cv-json' => [
		'file' => 'cv_json',
		'standalone' => true,
	],
	'kekkerit' => [
		'file' => '../kekkerit/index',
		'standalone' => true,
	],
];

$request = urlencode( $_GET['page'] ?? null );
$is_frontpage = false;

if ( empty( $request )  ) {
	$request = 'home';
	$is_frontpage = true;
	$blogs = get_blogs( 3 );
}

if ( array_key_exists( $request, $pages ) ) {
  $current_page = $pages[ $request ]['file'];
} else {
  $current_page = '404';
}

$title = $pages[ $request ]['title'];

$email = antispambot('anttoni@fastmail.com');

if ( $pages[ $request ]['standalone'] ?? false ) {
	include_once( 'files/' . $current_page . '.php' );
	exit;
}

include_once('files/head.php');
include_once('files/header.php');
include_once( 'files/' . $current_page . '.php' );
include_once( 'files/footer.php' );
