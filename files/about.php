<p>Nice to meet you.</p>

<p>I'm a what you would call a human-oriented developer. I like to write code wheter it's to create applications to solve business problems or just script something to make my life easier. For me, programming is way of self-expression and I love the problem solving (both in micro and macro sense)! But as much as I like programming, I live off of one-two-one conversations. Something to drink and a dear friend or new acquaintance - sign me up! <a href="coffee">Get in touch and lets chat!</a></p>

<h2> Pieces from personal history </h2>

<h3> Time in Hämeenlinna </h3>
<p>I was born in Hämeenlinna and spent my first 20 years there. My parents still live there and they with my super awesome sister remain important part of my life.<br> <i>Hämeenlinna is georaphically remarkable city as it's a one hour drive away from everywhere!</i></p>
<p>2007 was my first experience of web development as I took an IT-class in secondary school. <br> <i>Got 50 euro stipend being best student of that class!</i></p>
<p>Entrepreneurship got into me when I studied at Hämeenlinnan Yhteiskoulun lukio specializing in Creative Entrepreneurship. Started doing web, videos and simple graphic design as a sole trader while still in high scool.<br> <i> I got to interview Cheek and Fintelligens!</i></p>
<p>In 2012 I got a lot more video shooting and editing experience when I did my military service in Parola as multimedia conscript. <br> <i>They even promoted me to a lance corporal!</i></p>

<h3> Moved to Tampere</h3>
<p>In 2013 I moved to Tampere and started doing computer science bachelors degree at Tampere University of applied science.</p>
<p>In spring of 2016 I started working part-time in Geniem <a href="https://www.geniem.fi/in-english/">(check out the companys website!)</a>. After getting my bachelors degree in December I started working there full time.<br><i>As addition to the awesome people there, we had OG soda machine and pinball!</i></p>

<h3> Turku is my home now </h3>
<p>Fall 2018 I moved to Turku<br><i>Turku is like living abroad in Finland</i></p>
<p>Spring 2019 I got married with Viivi, the coolest person I know.</p>
<p>2019 new winds blow as I started doing my masters in Information System Science at Turku Shool of Economics. <i>I should be doing school work even now!</i></p>
