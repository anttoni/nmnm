<div class="wrap">
	<div class="intro">
		<p> I'm a developer who prefers people over computers. At the moment I study <i>business informatics</i> at Turku University of Economics. </p>
		<p>I think that Internet is the best collaborative achievement of human race and that the more open and accessible it is, better will our future become.</p>

	<p>On this website you'll find out:
	<ul>
		<li>what I do, and have done  <a href="/what-dones.html">(see my doings)</a>,</li>
		<li>what I think <a href="/writings.html">(check the blog)</a>, </li>
		<li>who I am <a href="/iam-anttoni">(read about me)</a>, </li>
		<li>and what I am interested about <a href="/interests.html">(take a look at my interests)</a> </li>
	</ul>
	<p>Oh, and BTW I use Arch.  <a href="dem-wares.html">(list of my harware and software)</a> </p>
	</div>
	<img src="img/anttoni.png" alt="">
</div>
	<h2> Some of my doings </h2>
	<ul>
		<li><i>Netvisor PHP API</i> - a PHP library for interacting with Netvisor's API.
			 <a href="https://gitlab.com/anttoni/netvisor-php-api"><br>(check out Netvisor PHP API on my GitLab)</a> 
		</li>
		<li><i>Kekkerit.fi</i> - Our biannual Moomin Marathon web HQ. Hacked together during weekend. 
			<a href="https://nmnm.fi/what-dones-kekkerit"><br>(read more about the website and Moomin marathon)</a> 
		</li>
		<li><i>Leijonaa.fi</i> - my dear friends blog powered by theme I created powered by DustPress
			 <a href="https://nmnm.fi/what-dones-leijonaa"><br>(read more about leijonaa.fi)</a>
		</li>
	</ul>
	<p> <a href="what-dones">See all of my doings</a></p> 
	<?php if( $blogs ): ?>
		<h2> Some of my writings </h2>
		<ul>
			<?php foreach( $blogs as $blog ):?>
				<li><a href="<?php echo $blog['url'] ?>"><?php echo $blog['title'] ?></a></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	<p><a href="writings.html">Read more of my writings</a></p> 
<h2> Contact </h2>
<p>Best (and currently only) way to contact me is via email: <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a> I read it daily and can get back to you quickly. I'm not in any social media platform  (<a href="writings/i-quit-social-media.html">read why I quit</a>).</p>
