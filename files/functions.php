<?php
/**
 * From Wordpress: https://developer.wordpress.org/reference/functions/antispambot
 */
function antispambot( $email_address, $hex_encoding = 0 ) {
    $email_no_spam_address = '';
    for ( $i = 0, $len = strlen( $email_address ); $i < $len; $i++ ) {
        $j = rand( 0, 1 + $hex_encoding );
        if ( $j == 0 ) {
            $email_no_spam_address .= '&#' . ord( $email_address[ $i ] ) . ';';
        } elseif ( $j == 1 ) {
            $email_no_spam_address .= $email_address[ $i ];
        } elseif ( $j == 2 ) {
            $email_no_spam_address .= '%' . zeroise( dechex( ord( $email_address[ $i ] ) ), 2 );
        }
    }

    return str_replace( '@', '&#64;', $email_no_spam_address );
}


function get_blogs( $max = 100 ) {
    $files = scandir( 'blogs' );
    $files = array_diff( $files, ['..', '.']);
    $blogs = [];
    $i = 1;
    foreach ( $files as $file ) {
        $blogs[] = [
            'url'  => '/blog/' . $file,
            'title' => substr( fgets( fopen( 'blogs/' . $file , 'r') ), 2 ),
        ];

        if( ++$i === $max ) {
            break;
        }
    }

    return $blogs;
}