<?php

$cv = [

	'Job experience' => [
		[
			'title' => 'Developer',
			'position' => 'Geniem Oy',
			'year' => '2016 - 2019',
		],
		[
			'title' => 'Sole trader',
			'position' => 'Web development, IT consulting, video production',
			'year' => '2012 - 2019',
		],
		[
			'title' => 'Co-founder of Kiivisio co-operative',
			'position' => 'Proakatemia',
			'year' => '2013-2016',
		],
	],

	'Education' => [
		[
			'title' => 'Masters ',
			'position' => 'Turku School of Economics',
			'year' => '2019 (started)',
		],
		[
			'title' => 'Bachelor of Business Administration in ICT-entrepreneurship and team-leading',
			'position' => 'Tampere University of Applied Sciences, Proakatemia',
			'year' => '2016',
		],
		[
			'title' => 'High School',
			'position' => 'Hämeenlinnan yhteiskoulun lukio',
			'year' => '2012',
		],
	],
	'Tech stack' => [
		[
			'title' => 'WordPress',
			'position' => 'Theme and plugin development',
			'year' => '+5 years of experience',
		],
		[
			'title' => 'WooCommerce',
			'position' => 'Theme and plugin development',
			'year' => '3 years of experience',
		],
		[
			'title' => 'PHP',
			'position' => 'Object-oriented development',
			'year' => '3 years of experience',
		],
		[
			'title' => 'HTML & CSS',
			'position' => 'CSS grid, flexbox, semantic HTML5',
			'year' => '+5 years of experience',
		],
	],
];

