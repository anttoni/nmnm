<a class="screen-reader-text jump-to-content" href="#content">
	Skip to content
</a>

<?php if( $is_frontpage ): ?>

<div class="contact">
Need help with web? Wanna say hi? Send me an email: <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
	<!--	| <a href="hire-me.html">Read about hiring me</a>-->
</div>

<?php else: ?>

<div class="fill"></div>

<?php endif;?>

<h1>Anttoni Niemenmaa</h1>
<nav>
  <?php foreach( $pages as $key => $page ): ?>
	<?php  if( array_key_exists( 'nav', $page )): ?>
		<a
		<?php echo ($key === $request) ? "class=\"current\"" : ""; ?>
		href="
		<?php echo ( $key === 'home' ) ? "/" : $key; ?>
		">
			<?php echo $page['nav']; ?>
		</a>
	<?php endif;?>
  <?php endforeach; ?>
</nav>
<h2 id="content"> <?php echo $title; ?></h2>


